/* eslint-disable react/prop-types */
import React, { Component,Fragment } from 'react'
import { Item, Label, Input, Icon,} from 'native-base';
import {Text} from 'react-native';

class PasswordInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            invisible: true
        }
    }
    render () {
        const { invisible } = this.state;
        return (
            <Fragment>
            <Item floatingLabel>
                <Label>{this.props.label}</Label>
                <Input secureTextEntry={invisible} onChangeText={this.props.onChangeText}/>
                <Icon name={ invisible ? "ios-eye-off" : "ios-eye" } onPress={() => this.setState({ invisible: !this.state.invisible })}/>
            </Item>
            {this.props.error !== '' && <Text style={{color:'red',marginLeft:10}}>{this.props.error}</Text>}
            </Fragment>
        )
    }
}

export default PasswordInput;