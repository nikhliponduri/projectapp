/* eslint-disable react/prop-types */
import React ,{Fragment}from "react";
import { Item, Label, Input, Icon } from "native-base";
import {Text} from 'react-native';

const FormInput = ({
  label,
  value,
  rightIcon,
  onChangeText,
  multiline,
  error
}) => {
  return (
    <Fragment>
    <Item floatingLabel>
      <Label>{label}</Label>
      <Input
        value={value}
        multiline={multiline || false}
        onChangeText={onChangeText}
      />
      {rightIcon && <Icon active name={rightIcon} />}
    </Item>
    {error !== '' && <Text style={{color:'red',marginLeft:10}}>{error}</Text>}
    </Fragment>
  );
};

export default FormInput;
