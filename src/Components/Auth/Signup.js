/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { Text, StyleSheet, ScrollView } from 'react-native';
import { Container, Form, Button, Content, Segment } from 'native-base';
import FormInput from '../shared/form-input';
import PasswordInput from '../shared/password-input'

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      cpassword: '',
      errors: false,
      singupType: 'user'
    };
  }
  render() {
    const { singupType } = this.state;
    return (
      <Container>
        <ScrollView>
          <Segment>
            <Button first onPress={() => this.setState({
              singupType: 'user'
            })} style={styles.segMentButton} active={singupType === 'user'}>
              <Text style={{...styles.segMentText}}>User</Text>
            </Button>
            <Button onPress={() => this.setState({
              singupType: 'merchant'
            })} style={styles.segMentButton} active={singupType === 'merchant'}>
              <Text style={{...styles.segMentText}}>Merchant</Text>
            </Button>
          </Segment>
          <Content style={styles.loginForm}>
            <Form>
              <FormInput label="First Name"
                rightIcon="ios-person"
                onChangeText={firstName => {
                  this.setState({ firstName });
                }}
                error={this.state.errors ? this.state.errors.firstName : ''}
              />
              <FormInput label="Last Name"
                rightIcon="ios-person"
                onChangeText={lastName => {
                  this.setState({ lastName });
                }}
                error={this.state.errors ? this.state.errors.lastName : ''}
              />
              {
                singupType === 'merchant' ? <FormInput label="Company"
                  rightIcon="ios-business"
                  onChangeText={company => {
                    this.setState({ company });
                  }}
                  error={this.state.errors ? this.state.errors.lastName : ''}
                /> : null
              }
              <FormInput label="Email"
                rightIcon="ios-mail"
                onChangeText={email => {
                  this.setState({ email });
                }}
                error={this.state.errors ? this.state.errors.email : ''}
              />
              <PasswordInput
                label="Password"
                onChangeText={password => {
                  this.setState({ password });
                }}
                error={this.state.errors ? this.state.errors.password : ''}
              />
              <PasswordInput label="Confirm Password"
                onChangeText={cpassword => {
                  this.setState({ cpassword });
                }}
                error={this.state.errors ? this.state.errors.cpassword : ''}
              />
              <Button block info style={styles.button} onPress={this.register}>
                <Text style={styles.buttonText}>Create My Account</Text>
              </Button>
              <Button block info style={styles.button}
                onPress={() => this.props.navigation.navigate('Login')}>
                <Text style={styles.buttonText}>Having An Account? Go to Login!</Text>
              </Button>
            </Form>
          </Content>
        </ScrollView>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    width: 100,
    height: 100
  },
  loginForm: {
    margin: 20
  },
  button: {
    marginTop: 10
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },
  facebookButton: {
    backgroundColor: '#3b5998'
  },
  googleButton: {
    backgroundColor: '#d34836'
  },
  twitterButton: {
    backgroundColor: '#38A1F3'
  },
  segMentButton: {
    paddingLeft:10,
    paddingRight:10,
    paddingBottom:5,
    paddingTop:5
  },
  segMentText: {
    fontWeight: 'bold'
  }
})

export default Register