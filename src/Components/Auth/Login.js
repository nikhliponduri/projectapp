/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native';
import { Container, Form, Button, Content, ListItem, CheckBox, Body } from 'native-base';
import FormInput from '../shared/form-input';
import PasswordInput from '../shared/password-input'

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
      errors: false
    };
  }
  render() {
    return (
      <Container>
        <Content>
          <View style={styles.logoContainer}>
          </View>
          <Text>{this.state.error}</Text>
          <View style={styles.loginForm}>
            <Form>
              <FormInput
                label="Email"
                rightIcon="ios-mail"
                onChangeText={email => {
                  this.setState({ email });
                }}
                error={this.state.errors ? this.state.errors.email : ''}
              />
              <PasswordInput
                label="Password"
                onChangeText={password => {
                  this.setState({ password });
                }}
                error={this.state.errors ? this.state.errors.password : ''}
              />
              <View style={styles.checkboxContainer}>
                <CheckBox
                  style={styles.checkbox}
                />
                <Text style={styles.label}>Login As Merchant</Text>
              </View>
              <Button block info style={styles.button} onPress={this.login}>
                <Text style={styles.buttonText}>Login</Text>
              </Button>
              <Button block info style={styles.button}
                onPress={() => this.props.navigation.navigate('SignUp')}>
                <Text style={styles.buttonText}>New Here? Proceed to SignUp!</Text>
              </Button>
            </Form>
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  logoContainer: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  logo: {
    width: 100,
    height: 100
  },
  loginForm: {
    flex: 1,
    margin: 20,
    justifyContent: 'center'
  },
  button: {
    marginTop: 10
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },
  twitterButton: {
    backgroundColor: '#38A1F3'
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
    marginTop:20
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    marginLeft: 15,
  },
})
