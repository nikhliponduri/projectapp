/* eslint-disable react/prop-types */
import React, { Component, Fragment } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { initialLoad } from '../reducers/auth';
import { Spinner } from 'native-base';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './Components/Home/index';
import Login from './Components/Auth/Login';
import SignUp from './Components/Auth/Signup';

const Stack = createStackNavigator();


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  async componentDidMount() {
    await this.props.initialLoad();
    this.setState({
      loading: false
    })
  }
  render() {
    const { loading } = this.state;
    const { auth } = this.props;
    if (loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Spinner />
          <Text>Please wait...</Text>
        </View>
      )
    }
    return (
      <NavigationContainer>
        <Stack.Navigator>
          {
            auth.isAuthenticated ? <Stack.Screen name="Home" component={Home} /> :
              <Fragment>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="SignUp" component={SignUp} />
              </Fragment>
          }
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default connect(({ auth }) => ({ auth }), { initialLoad })(Index)