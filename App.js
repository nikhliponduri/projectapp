import 'react-native-gesture-handler';
import React from 'react';
import Main from './src/index';
import { Provider } from 'react-redux';
import store from './store';
import { Root } from "native-base";

const App = () => {
  return (
    <Provider store={store}>
      <Root>
        <Main />
      </Root>
    </Provider>
  );
};

export default App;
