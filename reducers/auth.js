import { AsyncStorage } from 'react-native';

const inititalState = {
  isAuthenticated: false,
  session: null,
};

const SET_SESSION = 'SET_SESSION';

export const initialLoad = () => async (dispatch) => {
  const session = await AsyncStorage.getItem('@session');
  if (session) {
    dispatch({
      type: SET_SESSION,
      auth: {
        isAuthenticated: true,
        session
      }
    })
  }
}

export default (state = inititalState, action) => {
  switch (action.type) {
    case SET_SESSION:
      return action.auth
    default:
      return state;
  }
};
